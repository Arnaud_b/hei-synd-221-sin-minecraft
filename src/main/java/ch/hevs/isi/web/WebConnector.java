package ch.hevs.isi.web;
import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.Core.DataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;

public class WebConnector extends WebSocketServer {

    private static WebConnector wc = null;

    private WebConnector(){
        super(new InetSocketAddress(8888));
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        webSocket.send("Welcome on board ! Flight 821 to Hawaï");
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {

    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        String message[] = s.split("=");
        if(s.equals(true) || s.equals(false)) {
            DataPoint.getBooleanDataPointFromLabel(message[0]).setValue(Boolean.parseBoolean(message[1]));
        }else {
            DataPoint.getFloatDataPointFromLabel(message[0]).setValue(Float.parseFloat(message[1]));
        }
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {

    }

    @Override
    public void onStart() {

    }

    /**
     * The static method getInstance() returns a reference to the singleton
     * @return WebConnector
     */
    public static WebConnector getInstance(){
        if(wc == null){
            wc = new WebConnector();
        }
        return wc;
    }

    /**
     * call the send method when a DataPoint object's value has changed
     * @param fdp the FloatDataPoint object that had it's value changed
     */
    public void onNewValueFloat(FloatDataPoint fdp){
        System.out.println(fdp.getLabel() + " : " + fdp.getValue() + " pushed on the DB");

        for (WebSocket client:this.getConnections()) {
            if(client.isOpen()) {
                client.send(fdp.getLabel()+"="+String.valueOf(fdp.getValue()));
            }
        }
    }

    /**
     * call the send method when a DataPoint object's value has changed
     * @param bdp the BooleanDataPoint object that had it's value changed
     */
    public void onNewValueBoolean(BooleanDataPoint bdp){
        System.out.println(bdp.getLabel() + " : " + bdp.getValue() + " pushed on the DB");

        for (WebSocket client:this.getConnections()) {
            if(client.isOpen()) {
                client.send(bdp.getLabel()+"="+String.valueOf(bdp.getValue()));
            }
        }

    }

}

