package ch.hevs.isi.field;
import ch.hevs.isi.Core.FloatDataPoint;
import java.util.HashMap;

public class FloatRegister {

    private int address;
    private static HashMap<FloatDataPoint, FloatRegister> _dpMap = new HashMap<>();
    private FloatDataPoint datapoint;
    private int offset;
    private int range;

    /**
     * FloatRegister Constructor
     *
     * @param isOutput this variable is true if the variable is used as output
     * @param address the Modbus address related to the data represented by the DataPoint
     * @param label the name of the DataPoint object
     */
    public FloatRegister(boolean isOutput, int address, String label, int offset, int range) {
        this.address = address;
        datapoint = new FloatDataPoint(label, isOutput);
        _dpMap.put(this.datapoint, this);
        this.offset = offset;
        this.range = range;
    }

    /**
     * read the value in the server's register at the right address through Modbus and set it in the FloatDataPoint
     */
    public void read() {
        datapoint.setValue(ModbusAccessor.getInstance().readFloat(address)*range + offset);
    }

    /**
     * write the value of the FloatDataPoint in the server's register through Modbus
     */
    public void write() {
        ModbusAccessor.getInstance().writeFloat(address, (datapoint.getValue()-offset) / range);
    }

    /**
     * Use to get the FloatRegister that contains the bdp param
     * @param fdp
     * @return FloatRegister
     */
    public static FloatRegister getRegisterFromDatapoint(FloatDataPoint fdp){
        return  _dpMap.get(fdp);
    }

    /**
     * call the read method on all the FloatRegisters
     */
    public static void poll(){
        for(int i = 0; i< _dpMap.size();i++){
            FloatRegister fr = (FloatRegister) _dpMap.values().toArray()[i];
            fr.read();
        }
    }
}