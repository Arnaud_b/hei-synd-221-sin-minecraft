package ch.hevs.isi.field;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

public class ModbusAccessor {

    private static ModbusAccessor ma = null;
    private ModbusMaster _modbus;


    /**
     * The static method getInstance() returns a reference to the singleton
     * @return ModbusAccessor
     */
    public static ModbusAccessor getInstance() {
        if (ma == null) {
            ma = new ModbusAccessor();
        }
        return ma;
    }

    private ModbusAccessor() {}

    /**
     * Establish a connection by indicate the port number and the host name
     * @param host the name of the host
     * @param port the number of the server port to connect to
     */
    public void connect(String host, Integer port) {
        IpParameters params = new IpParameters();
        params.setHost(host);
        params.setPort(port);
        _modbus = new ModbusFactory().createTcpMaster(params, false);
        try {
            _modbus.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param address address of the modbus register to read
     * @return boolean value
     */
    public boolean readBoolean(int address) {
        try {
            return _modbus.getValue(BaseLocator.coilStatus(1, address)).booleanValue();
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param address address of the modbus register to read
     * @return float value
     */
    public float readFloat(int address) {
        try {
            return _modbus.getValue(BaseLocator.inputRegister(1, address, DataType.FOUR_BYTE_FLOAT)).floatValue();
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * @param address address of the modbus register
     * @param value the boolean value to write in the register
     */
    public void writeBoolean(int address, boolean value) {
        try {
            _modbus.setValue(BaseLocator.coilStatus(1, address), value);
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param address address of the modbus register
     * @param value the float value to write in the register
     */
    public void writeFloat(int address, float value) {
        try {
            _modbus.setValue(BaseLocator.holdingRegister(1, address, DataType.FOUR_BYTE_FLOAT), value);
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
    }
}
