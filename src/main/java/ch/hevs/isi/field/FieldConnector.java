package ch.hevs.isi.field;
import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import java.util.Timer;
import java.util.TimerTask;

public class FieldConnector {

    private static FieldConnector fc = null;

    /**
     * The static method getInstance() returns a reference to the singleton
     * @return FieldConnector
     */
    public static FieldConnector getInstance(){
        if(fc == null){
            fc = new FieldConnector();
        }
        return fc;
    }

    /**
     * call the write method when a FloatDataPoint object's value has changed
     * @param fdp
     */
    public void onNewValueFloat(FloatDataPoint fdp){
        FloatRegister fr = FloatRegister.getRegisterFromDatapoint(fdp);
        System.out.println(fdp.getLabel() + " : " + fdp.getValue() + " entered on the DB");
        fr.write();
        System.out.println(fdp.getLabel() + " : " + fdp.getValue() + " entered on the DB");
    }

    /**
     * call the write method when a BooleanDataPoint object's value has changed
     * @param bdp
     */
    public void onNewValueBoolean(BooleanDataPoint bdp){
        BooleanRegister br = BooleanRegister.getRegisterFromDatapoint(bdp);
        br.write();
        System.out.println(bdp.getLabel() + " : " + bdp.getValue() + " entered on the DB fc");
    }

    /**
     * Updated all the registers every nTime by using the poll() method
     * @param nTime
     */
    public void startPollIn(int nTime){
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run(){
                BooleanRegister.poll();
                FloatRegister.poll();
            }
        }, nTime, nTime);
    }

    /**
     * @param address the Modbus address related to the data represented by the DataPoint
     * @param label the name of the BooleanDataPoint object
     * @param isOutput this variable is true if the variable is used as output
     */
    public void addBooleanRegister(int address, String label, boolean isOutput, boolean value){
        BooleanRegister br = new BooleanRegister(isOutput, address, label,value );
        DataPoint bdp = BooleanDataPoint.getBooleanDataPointFromLabel(label);
    }

    /**
     * @param address the Modbus address related to the data represented by the DataPoint
     * @param label the name of the BooleanDataPoint object
     * @param isOutput this variable is true if the variable is used as output
     */
    public void addFloatRegister(int address, String label, boolean isOutput, int offset, int range){
        FloatRegister fr = new FloatRegister(isOutput, address, label, offset, range);
        DataPoint fdp = FloatDataPoint.getFloatDataPointFromLabel(label);
    }
}
