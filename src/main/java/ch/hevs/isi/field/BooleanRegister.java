package ch.hevs.isi.field;
import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import java.util.HashMap;

public class BooleanRegister {

    private int address;
    private BooleanDataPoint datapoint;
    private static HashMap<BooleanDataPoint, BooleanRegister> _dpMap = new HashMap<>();

    /**
     * BooleanRegister Constructor
     *
     * @param isOutput this variable is true if the variable is used as output
     * @param address the Modbus address related to the data represented by the DataPoint
     * @param label the name of the DataPoint object
     */
    public BooleanRegister(boolean isOutput, int address, String label,boolean value) {
        this.address = address;
        datapoint = new BooleanDataPoint(label, isOutput,value);
       _dpMap.put(this.datapoint, this);
    }

    /**
     * read the value in the server's register at the right address through Modbus and set it in the BooleanDataPoint
     */
    public void read() {

        datapoint.setValue(ModbusAccessor.getInstance().readBoolean(address));
    }

    /**
     * Write the value of the BooleanDataPoint in the server's register through Modbus
     */
    public void write() {

        ModbusAccessor.getInstance().writeBoolean(address, datapoint.getValue());
    }

    /**
     * Use to get the BooleanRegister that contains the bdp param
     * @param bdp a BooleanDataPoint object
     * @return BooleanRegister
     */
    public static BooleanRegister getRegisterFromDatapoint(BooleanDataPoint bdp){

        return  _dpMap.get(bdp);
    }

    /**
     * call the read method on all the BooleanRegisters
     */
    public static void poll(){

        for(int i = 0; i< _dpMap.size();i++){
            BooleanRegister br = (BooleanRegister) _dpMap.values().toArray()[i];
            br.read();
        }
    }

}