package ch.hevs.isi.Core;
import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import java.util.HashMap;

public abstract class DataPoint{
    public enum Direction{

        INPUT,

        OUTPUT
    }

    private String _label;
    private boolean _isOutput;

    protected final static DatabaseConnector dbc = DatabaseConnector.getInstance();
    protected final static WebConnector wc = WebConnector.getInstance();
    protected final static FieldConnector fc =  FieldConnector.getInstance();

    private static HashMap<String, DataPoint> _dpMap = new HashMap<>();

    /**
     * DataPoint constructor
     *
     * @param label the name of the DataPoint object
     * @param isOutput this variable is true if the variable is used as output
     */
    protected DataPoint(String label, boolean isOutput){
        this._label = label;
       this._isOutput = isOutput;
      _dpMap.put(label,  this);
    }

    /**
     * abstract setValue
     * @param value the value of the DataPoint object
     */
    public abstract void setValue(Object value);

    /**
     * get the name of the DataPoint object
     * @return _label
     */
    public String getLabel() {
        return _label;
    }

    /**
     * indicate if the variable is an output
     * @return _isOutput boolean
     */
    public boolean isOutput() {
        return _isOutput;
    }

    /**
     * get the DataPoint object from the HashMap by using it's key
     * @param label the name of the DataPoint object
     * @return DataPoint
     */
    private static DataPoint getDataPointFromLabel(String label){
        return _dpMap.get(label);
    }

    /**
     * get the Float DataPoint object from the HashMap by using it's key
     * @param label the name of the DataPoint object
     * @return DataPoint or null
     */
    public static DataPoint getFloatDataPointFromLabel(String label) {
       DataPoint check = _dpMap.get(label);
       if(check instanceof FloatDataPoint){
           return check;
       }else{
           return null;
       }
    }

    /**
     * get the Boolean DataPoint object from the HashMap by using it's key
     * @param label the name of the DataPoint object
     * @return DataPoint or null
     */
    public static DataPoint getBooleanDataPointFromLabel(String label) {
        DataPoint check = _dpMap.get(label);
        if(check instanceof BooleanDataPoint){
            return check;
        }else{
            return null;
        }
    }
}
