package ch.hevs.isi.Core;

public class BooleanDataPoint extends DataPoint {

    private boolean _value;

    /**
     * BooleanDataPoint constructor
     *
     * @param _label the name of the BooleanDataPoint object
     * @param direction same role as isOutput variable, to know if the variable is an output
     * @param value the value of the BooleanDataPoint object
     */
    public BooleanDataPoint(String _label, boolean direction, boolean value ){
        super(_label, direction);
        _value = value;
    }

    /**
     * Set the value of the BooleanDataPoint
     * @param value the new value of the DataPoint object
     */
    @Override
    public void setValue(Object value) {
        if(value instanceof Boolean){
            this._value=((Boolean) value).booleanValue();
            dbc.onNewValueBoolean(this);
            wc.onNewValueBoolean(this);
            if(isOutput()){
                fc.onNewValueBoolean(this);
            }
        }
    }

    /**
     * return the value attribute of the BooleanDataPoint object
     * @return _value
     */
    public boolean getValue(){
        return this._value;
    }


}
