package ch.hevs.isi.Core;

public class FloatDataPoint extends DataPoint {

    private float _value;

    /**
     * FloatDataPoint Constructor
     *
     * @param _label the name of the BooleanDataPoint object
     * @param direction same role as isOutput variable, to know if the variable is an output
     */
    public FloatDataPoint(String _label, boolean direction ){
        super(_label, direction);
    }

    /**
     * Set the value of the FloatDataPoint
     * @param value the new value of the DataPoint object
     */
    @Override
    public void setValue(Object value) {
        if(value instanceof Float){
            this._value=((Float) value).floatValue();
            dbc.onNewValueFloat(this);
            wc.onNewValueFloat(this);
            if(isOutput()){
                fc.onNewValueFloat(this);
            }
        }
    }

    /**
     * return the value attribute of the FloatDataPoint object
     * @return _value
     */
    public float getValue(){
        return this._value;
    }

}
