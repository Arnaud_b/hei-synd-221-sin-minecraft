package ch.hevs.isi;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.SmartControl.SmartControl;
import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.FloatRegister;
import ch.hevs.isi.field.ModbusAccessor;
import ch.hevs.isi.utils.Utility;
import ch.hevs.isi.web.WebConnector;


public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <ModbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "root";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }

        ModbusAccessor.getInstance().connect(modbusTcpHost, modbusTcpPort );

        WebConnector.getInstance().start();

        FieldConnector.getInstance().addFloatRegister(89, "GRID_U_FLOAT", false, 0, 1000);
        FieldConnector.getInstance().addFloatRegister(57, "BATT_P_FLOAT", false, -3000, 6000);
        FieldConnector.getInstance().addFloatRegister(49, "BATT_CHRG_FLOAT", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(61, "SOLAR_P_FLOAT", false, 0, 1500);
        FieldConnector.getInstance().addFloatRegister(53, "WIND_P_FLOAT", false, 0, 1000);
        FieldConnector.getInstance().addFloatRegister(81, "COAL_P_FLOAT", false, 0, 600);
        FieldConnector.getInstance().addFloatRegister(65, "COAL_AMOUNT", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(101, "HOME_P_FLOAT", false, 0, 1000);
        FieldConnector.getInstance().addFloatRegister(97, "PUBLIC_P_FLOAT", false, 0, 500);
        FieldConnector.getInstance().addFloatRegister(105, "FACTORY_P_FLOAT", false, 0, 2000);
        FieldConnector.getInstance().addFloatRegister(93, "BUNKER_P_FLOAT", false, 0, 500);
        FieldConnector.getInstance().addFloatRegister(301, "WIND_FLOAT", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(305, "WEATHER_FLOAT", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(309, "WEATHER_FORECAST_FLOAT", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(313, "WEATHER_COUNTDOWN_FLOAT", false, 0, 600);
        FieldConnector.getInstance().addFloatRegister(317, "CLOCK_FLOAT", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(209, "REMOTE_COAL_SP", true, 0, 1);
        FieldConnector.getInstance().addFloatRegister(205, "REMOTE_FACTORY_SP", true, 0, 1);
        FieldConnector.getInstance().addBooleanRegister(401,"REMOTE_SOLAR_SW", true,true);
        FieldConnector.getInstance().addBooleanRegister(405,"REMOTE_WIND_SW", true,true);
        FieldConnector.getInstance().addFloatRegister(341, "FACTORY_ENERGY", false, 0, 3600000);
        FieldConnector.getInstance().addFloatRegister(345, "SCORE", false, 0, 3600000);
        FieldConnector.getInstance().addFloatRegister(601, "COAL_SP", false, 0, 1);
        FieldConnector.getInstance().addFloatRegister(605, "FACTORY_SP", false, 0, 1);
        FieldConnector.getInstance().addBooleanRegister(609,"SOLAR_CONNECT_SW", false,true);
        FieldConnector.getInstance().addBooleanRegister(613,"WIND_CONNECT_SW", false,true);

        FieldConnector.getInstance().startPollIn(1000);
        SmartControl.getInstance().startControl(1000);
    }
}
