package ch.hevs.isi.database;
import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;

public class DatabaseConnector {

    private static String userpass = "SIn15:229a29f44e110c7299bf460c056ff372";
    private static DatabaseConnector dbc = new DatabaseConnector();

    private DatabaseConnector() {}

    /**
     *create our url
     */
    private static URL url;
    static {
        try{
            url = new URL("https://influx.sdi.hevs.ch/write?db=SIn15");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Sends an HTTP POST to the database with the given body
     *
     * @param body the message body  to be posted
     * @throws IOException
     */
    private void send (String body) throws IOException{
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
        connection.setRequestProperty ("Authorization", "Basic " + encoding);
        connection.setRequestProperty("Content-Type", "binary/octet-stream");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

        writer.write(body);
        writer.flush();

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((in.readLine()) != null) {}

        in.close();
        writer.close();
        connection.disconnect();
    }

    /**
     * The static method getInstance() returns a reference to the singleton
     *
     * @return DatabaseConnector
     */
    public static DatabaseConnector getInstance(){
        if(dbc == null){
            dbc = new DatabaseConnector();
        }
        return dbc;
    }

    /**
     * calls the pushToDataBase method when a DataPoint object's value gets changed
     * @param fdp the FloatDataPoint wich have its value changed
     */
    public void onNewValueFloat(FloatDataPoint fdp){
        System.out.println(fdp.getLabel() + " : " + fdp.getValue() + " entered on the DB");
        pushToDatabaseFloat(fdp.getLabel(), fdp.getValue());
    }

    /**
     * calls the pushToDataBase method when a DataPoint object's value gets changed
     * @param bdp the BooleanDataPoint wich have its value changed
     */
    public void onNewValueBoolean(BooleanDataPoint bdp){
        System.out.println(bdp.getLabel() + " : " + bdp.getValue() + " entered on the DB");
        pushToDatabaseBoolean(bdp.getLabel(), bdp.getValue());
    }

    /**
     * Push the new value of the float point with the given name to the DataBase
     * @param _label the name of the DataPoint object
     * @param value the value of the DataPoint
     * @throws IOException
     */
    public void pushToDatabaseFloat(String _label,float value){
        String body = _label + " value=" + Float.toString(value);
       try{
           send(body);
       }catch(IOException e){
           e.printStackTrace();
        }
    }

    /**
     * Push the new value of the boolean point with the given name to the DataBase
     * @param _label the name of the DataPoint object
     * @param value the value of the DataPoint
     * @throws IOException
     */
    public void pushToDatabaseBoolean(String _label, boolean value){
        String body = _label + " value=" + Boolean.toString(value);
        try{
            send(body);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
