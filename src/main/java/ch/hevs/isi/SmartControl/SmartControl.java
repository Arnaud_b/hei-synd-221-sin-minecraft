package ch.hevs.isi.SmartControl;
import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import java.util.Timer;
import java.util.TimerTask;

public class SmartControl {
    private static SmartControl sm = new SmartControl(); //singleton

    public static SmartControl getInstance(){
        return sm;
    }

    private SmartControl (){

    }

    /**
     * startControl is a polling method for a continuous regulation
     * @param nTime
     */
    public void startControl(int nTime){
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run(){
               doControl();
            }
        }, nTime, nTime);
    }

    /**
     * doControl is the regulation method
     */
    private void doControl() {
        FloatDataPoint REMOTE_FACTORY_SP = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("REMOTE_FACTORY_SP");
        BooleanDataPoint REMOTE_SOLAR_SW = (BooleanDataPoint) DataPoint.getBooleanDataPointFromLabel("REMOTE_SOLAR_SW");
        BooleanDataPoint REMOTE_WIND_SW = (BooleanDataPoint) DataPoint.getBooleanDataPointFromLabel("REMOTE_WIND_SW");
        FloatDataPoint REMOTE_COAL_SP = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("REMOTE_COAL_SP");
        FloatDataPoint BATT_CHRG_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("BATT_CHRG_FLOAT");
        FloatDataPoint SOLAR_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("SOLAR_P_FLOAT");
        FloatDataPoint SCORE = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("SCORE");
        FloatDataPoint CLOCK_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("CLOCK_FLOAT");
        FloatDataPoint HOME_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("HOME_P_FLOAT");
        FloatDataPoint FACTORY_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("FACTORY_P_FLOAT");
        FloatDataPoint BUNKER_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("BUNKER_P_FLOAT");
        FloatDataPoint PUBLIC_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("PUBLIC_P_FLOAT");
        FloatDataPoint WIND_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("WIND_P_FLOAT");
        FloatDataPoint COAL_P_FLOAT = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("COAL_P_FLOAT");

        REMOTE_SOLAR_SW.setValue(true);
        REMOTE_WIND_SW.setValue(true);
        System.out.println("Score: "+ SCORE.getValue());
        System.out.println("Time: " + CLOCK_FLOAT.getValue()*24);

        if (CLOCK_FLOAT.getValue() < 0.25f || CLOCK_FLOAT.getValue() > 0.75f) /*night*/ {
            REMOTE_FACTORY_SP.setValue(0.3f);

            if (BATT_CHRG_FLOAT.getValue() < 0.5f) {
                REMOTE_COAL_SP.setValue(1f);
            } else {
                if (BATT_CHRG_FLOAT.getValue() < 0.7f) {
                    float sp = (-SOLAR_P_FLOAT.getValue() + HOME_P_FLOAT.getValue() + FACTORY_P_FLOAT.getValue() + BUNKER_P_FLOAT.getValue() + PUBLIC_P_FLOAT.getValue() - WIND_P_FLOAT.getValue()) / 600f;
                    if (sp > 0f) {
                        if (sp < 1f) {
                            REMOTE_COAL_SP.setValue(sp);
                        } else {
                            REMOTE_COAL_SP.setValue(1f);
                        }
                    } else {
                        REMOTE_COAL_SP.setValue(0f);
                    }
                } else {
                    REMOTE_COAL_SP.setValue(0f);
                    REMOTE_FACTORY_SP.setValue(1f);
                }
            }
        } else /*day*/ {
            REMOTE_COAL_SP.setValue(0f);
            if (BATT_CHRG_FLOAT.getValue()>0.85f){
                REMOTE_FACTORY_SP.setValue(1f);
            }else{
                if (BATT_CHRG_FLOAT.getValue()>0.5f){
                    float sp = (SOLAR_P_FLOAT.getValue() -HOME_P_FLOAT.getValue()+ COAL_P_FLOAT.getValue() - BUNKER_P_FLOAT.getValue() - PUBLIC_P_FLOAT.getValue() + WIND_P_FLOAT.getValue()) / 1000f;
                    if (BATT_CHRG_FLOAT.getValue()<0.6f){
                        sp = sp-0.05f;
                    }
                    if (sp > 0f) {
                        if (sp < 1f) {
                            REMOTE_FACTORY_SP.setValue(sp);
                        } else {
                            REMOTE_FACTORY_SP.setValue(1f);
                        }
                    } else {
                        REMOTE_FACTORY_SP.setValue(0f);
                    }
                }else{
                    REMOTE_FACTORY_SP.setValue(0f);
                }
            }

        }
    }



}
